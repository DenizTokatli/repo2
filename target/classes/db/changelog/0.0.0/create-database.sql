-- create schema
CREATE SCHEMA rt
GO

--create tables
create table rt.activity
(
    id   int identity
        constraint activity_pk
            primary key nonclustered,
    name varchar(512)
)
go

--create indexes
create unique index activity_id_uindex
    on rt.activity (id)
go

