package com.dnzGym.routinetrackerapi.activity

import com.dnzGym.routinetrackerapi.activity.persistence.ActivityMapper
import spock.lang.Specification

class ActivityServiceSpec extends Specification {
    ActivityMapper mockActivityMapper = Mock(ActivityMapper)
    ActivityService activityService = new ActivityService(activityMapper: mockActivityMapper)

    def "Should insert activity"() {
        given:
        def activity = new Activity(123,'abc')
        def expectedResult = new Activity(123,'abc')

        when:
        def result = activityService.insert(activity)

        then:
        1 * mockActivityMapper.insert(activity)
        result == expectedResult
    }

    def "Should find all activity"() {
        given:
        def activity1 = new Activity(123,'abc')
        def activity2 = new Activity(456,'dfe')
        def expectedResult = [activity1, activity2]

        when:
        def result = activityService.findAll()

        then:
        1 * mockActivityMapper.findAll() >> expectedResult
        result == expectedResult
    }

}
