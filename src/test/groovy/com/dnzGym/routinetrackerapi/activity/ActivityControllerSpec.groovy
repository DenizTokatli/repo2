package com.dnzGym.routinetrackerapi.activity

import spock.lang.Specification

class ActivityControllerSpec extends Specification {
    ActivityService mockActivityService = Mock(ActivityService)
    ActivityController activityController = new ActivityController(activityService: mockActivityService)

    def "Should insert activity"() {
        given:
        def activity = new Activity()
        def expectedResult = new Activity()

        when:
        def result = activityController.insert(activity)

        then:
        1 * mockActivityService.insert(activity) >> expectedResult
        result == expectedResult
    }

    def "Should find all activity"() {
        given:
        def expectedResult = [new Activity()]

        when:
        def result = activityController.findAll()

        then:
        1 * mockActivityService.findAll() >> expectedResult
        result == expectedResult
    }

}
