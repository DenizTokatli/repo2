package com.dnzGym.routinetrackerapi.activity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;

@CrossOrigin // allow cross-origin domain request: testing
@RestController
@RequestMapping("/activities")
public class ActivityController {
    Logger logger = LoggerFactory.getLogger(ActivityController.class);

    @Autowired
    private ActivityService activityService;

    @GetMapping
    public List<Activity> findAll() { return activityService.findAll();}

    @PostMapping
    public Activity insert(@RequestBody Activity activity) {
        logger.info(activity.getName());
        return activityService.insert(activity);
    }

}
