package com.dnzGym.routinetrackerapi.activity.persistence;

import com.dnzGym.routinetrackerapi.activity.Activity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

@Mapper
public interface ActivityMapper {

    void insert(@Param("activity") Activity activity);

    List<Activity> findAll();

}
