package com.dnzGym.routinetrackerapi.activity;

import com.dnzGym.routinetrackerapi.activity.persistence.ActivityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivityService {

    @Autowired
    ActivityMapper activityMapper;

    public Activity insert(Activity activity) { activityMapper.insert(activity);return activity;}

    public List<Activity> findAll() { return activityMapper.findAll();}

}
