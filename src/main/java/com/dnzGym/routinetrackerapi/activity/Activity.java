package com.dnzGym.routinetrackerapi.activity;

import lombok.Data;

@Data // lombok.Data: bundles @Getter, @Setter, @ToString, etc...
public class Activity {

    private Long id;
    private String name;

    public Activity() {}

    public Activity(long id, String name) {
        this.id = id;
        this.name = name;
    }

}
